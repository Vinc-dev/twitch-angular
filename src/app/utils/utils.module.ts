import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingIconComponent } from './components/loading-icon/loading-icon.component';
import { WithLoadingPipe } from './pipes/with-loading.pipe';
import { NavbarComponent } from './components/navbar/navbar.component';
import {RouterModule} from "@angular/router";




@NgModule({
  declarations: [LoadingIconComponent, WithLoadingPipe, NavbarComponent],
  exports: [
    LoadingIconComponent,
    WithLoadingPipe,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class UtilsModule { }
