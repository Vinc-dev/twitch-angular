import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'twi-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  searchImg: any = '../assets/search.png';
  topImg: any = '../assets/top.png';

  constructor() { }

  ngOnInit(): void {
  }

}
