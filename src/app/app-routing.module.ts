import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)},
  {path: 'games/:idGame/streams', loadChildren: () => import('./streams/streams.module').then(m => m.StreamsModule)},
  {path: 'games/top', loadChildren: () => import('./games/games.module').then(m => m.GamesModule)},
  {path: 'games/search' , loadChildren: () => import('./searching/searching.module').then(m => m.SearchingModule)},
  {
    path: 'games/:idGame/streams/user-stream/:userName',
    loadChildren: () => import('./streams/streams.module').then(m => m.StreamsModule)
  },
  {path: '' , loadChildren: () => import('./home-page/home-page.module').then(m => m.HomePageModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload',
      scrollPositionRestoration: 'enabled'
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
