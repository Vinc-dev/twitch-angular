import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../auth/services/auth.service';
import {Injectable} from '@angular/core';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.authService.getToken();

    const cloneReq = req.clone({
      headers: req.headers
        .set('Client-ID', environment.api.clientId)
        .set('Authorization', `Bearer ${token}`),
      url: environment.api.baseUrl + req.url
    });
    return next.handle(cloneReq);
  }
}
