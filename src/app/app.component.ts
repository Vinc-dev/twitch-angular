import { Component } from '@angular/core';

@Component({
  selector: 'twi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'twitch-apli';
}
