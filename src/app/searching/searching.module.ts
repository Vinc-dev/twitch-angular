import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchingRoutingModule } from './searching-routing.module';
import { SearchPageComponent } from './components/search-page/search-page.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import {DataService} from './services/data.service';
import {UtilsModule} from '../utils/utils.module';
import {GamesModule} from '../games/games.module';
import {FormsModule} from "@angular/forms";




@NgModule({
  declarations: [SearchPageComponent, SearchBarComponent],
  imports: [
    CommonModule,
    SearchingRoutingModule,
    UtilsModule,
    GamesModule,
    FormsModule
  ],
  providers: [DataService]
})
export class SearchingModule { }
