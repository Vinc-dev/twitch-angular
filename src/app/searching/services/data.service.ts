import {Injectable, Input} from '@angular/core';
import {Game} from '../../games/Game';
import {TwitchService} from '../../api/services/twitch.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public gamesData: Game[];

  constructor(private twitchService: TwitchService) {}

  getGames(apiPath: string): Observable<Game[]>{
    return this.twitchService.getAll(apiPath);
  }
}
