import { Component, OnInit } from '@angular/core';
import {Game} from '../../../games/Game';
import {Observable} from 'rxjs';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'twi-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {

  games$: Observable<Game[]>;
  firstArrival: boolean;

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.games$ = new Observable<Game[]>();
    this.firstArrival = true;
  }

  searchThis(data) {
    this.firstArrival = false;
    this.games$ = this.dataService.getGames('/search/categories?query=' + data);
  }

}
