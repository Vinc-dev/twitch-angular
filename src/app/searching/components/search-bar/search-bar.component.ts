import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'twi-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {


  @Output() searchcriteria = new EventEmitter<string>();

  public searchword: string;

  constructor() {
  }

  searchThis() {
    this.searchcriteria.emit(this.searchword);
  }

  ngOnInit(): void {
  }
}
