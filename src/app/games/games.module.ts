import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GamesRoutingModule } from './games-routing.module';
import {GameComponent} from './game/game.component';
import {GamesTopComponent} from './games-top/games-top.component';
import {StreamsModule} from '../streams/streams.module';
import {UtilsModule} from '../utils/utils.module';


@NgModule({
	declarations: [GamesTopComponent, GameComponent],
	exports: [
		GameComponent
	],
	imports: [
		CommonModule,
		GamesRoutingModule,
		StreamsModule,
		UtilsModule
	]
})
export class GamesModule { }
