import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game} from '../Game';

@Component({
  selector: 'twi-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  @Input() game;

  ngOnInit(): void {
  }

}
