import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesTopComponent } from './games-top.component';

describe('GamesTopComponent', () => {
  let component: GamesTopComponent;
  let fixture: ComponentFixture<GamesTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamesTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
