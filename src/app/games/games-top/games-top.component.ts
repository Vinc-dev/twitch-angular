import {Component, OnInit} from '@angular/core';
import {TwitchService} from '../../api/services/twitch.service';
import {Observable} from 'rxjs';
import {Game} from '../Game';
import {Pipe} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {TwitchResponse} from '../../api/models/twitch-response';
import {pluck} from 'rxjs/operators';

@Component({
  selector: 'twi-games-top',
  templateUrl: './games-top.component.html',
  styleUrls: ['./games-top.component.scss']
})
export class GamesTopComponent implements OnInit {

  topGames$: Observable<Game[]>;

  constructor( private twitchService: TwitchService, private router: Router, private http: HttpClient) {
  }


  ngOnInit(): void {
    this.topGames$ = this.twitchService.getAll('/games/top');
  }

}
