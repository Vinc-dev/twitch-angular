import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GamesTopComponent} from './games-top/games-top.component';

const routes: Routes = [
  {path: '', component: GamesTopComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GamesRoutingModule { }
