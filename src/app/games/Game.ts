export interface Game {
  id: number;
  name: string;
  box_art_url: string;
}

