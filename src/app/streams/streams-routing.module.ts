import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StreamsOfGameComponent} from './streams-of-game/streams-of-game.component';
import {StreamViewComponent} from './component/stream-view/stream-view.component';

const routes: Routes = [
  {path : '', component: StreamsOfGameComponent},
  {path : 'stream', component: StreamViewComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StreamsRoutingModule { }
