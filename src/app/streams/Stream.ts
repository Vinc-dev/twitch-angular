export interface Stream {
  id: number;
  game_name: string;
  title: string;
  viewer_count: number;
  user_name: string;
  user_login: string;
  user_id: number;
  started_at: Date;
  language: string;
  thumbnail_url: string;
  game_id: number;
}
