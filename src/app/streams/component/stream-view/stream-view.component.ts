import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Game} from '../../../games/Game';
import {TwitchService} from '../../../api/services/twitch.service';
import {Observable} from 'rxjs';
import {User} from "../../User";

@Component({
  selector: 'twi-stream-view',
  templateUrl: './stream-view.component.html',
  styleUrls: ['./stream-view.component.scss']
})
export class StreamViewComponent implements OnInit {

  userName: string;
  game$: Observable<Game>;
  gameTitle: string;
  gameImg: string;
  gameId: string;
  user$: Observable<User>;
  descrUser: string;
  profileImgUser: string;
  userViewCount: number;

  constructor(
    private route: ActivatedRoute,
    private twitchService: TwitchService
  ) { }

  ngOnInit(): void {
    this.userName = this.route.snapshot.paramMap.get('userName');
    this.gameId = this.route.snapshot.paramMap.get('idGame');
    this.game$ = this.twitchService.getOne('/games?id=' + this.gameId);
    this.game$.subscribe(value => this.gameTitle = value.name);
    this.game$.subscribe(value => this.gameImg = value.box_art_url);
    this.user$ = this.twitchService.getOne('/users?login=' + this.userName);
    this.user$.subscribe(value => this.descrUser = value.description);
    this.user$.subscribe(value => this.profileImgUser = value.profile_image_url);
    this.user$.subscribe(value => this.userViewCount = value.view_count);
  }

}
