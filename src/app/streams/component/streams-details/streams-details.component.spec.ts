import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamsDetailsComponent } from './streams-details.component';

describe('StreamsDetailsComponent', () => {
  let component: StreamsDetailsComponent;
  let fixture: ComponentFixture<StreamsDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StreamsDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
