import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'twi-streams-details',
  templateUrl: './streams-details.component.html',
  styleUrls: ['./streams-details.component.scss']
})
export class StreamsDetailsComponent implements OnInit {

  @Input() stream;

  ngOnInit(): void {
  }

}
