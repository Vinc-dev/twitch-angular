import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'twi-tchat',
  templateUrl: './tchat.component.html',
  styleUrls: ['./tchat.component.scss']
})
export class TchatComponent implements OnChanges {

  @Input() userName: string;
  url: string;

  ngOnChanges(changes: SimpleChanges): void {
    this.url = environment.twitch.chatRoomUrl.replace('[USERNAME]', changes.userName.currentValue);
    console.log('this.url', this.url);
  }

}
