import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StreamsRoutingModule } from './streams-routing.module';
import { StreamsDetailsComponent } from './component/streams-details/streams-details.component';
import {StreamsOfGameComponent} from './streams-of-game/streams-of-game.component';
import {ReplaceUrlPipe} from './pipes/replace-url.pipe';
import {UtilsModule} from '../utils/utils.module';
import {VideoPlayerComponent} from './component/video-player/video-player.component';
import { StreamViewComponent } from './component/stream-view/stream-view.component';
import {SafePipeModule} from 'safe-pipe';
import {NgxFlagIconCssModule} from 'ngx-flag-icon-css';
import { TchatComponent } from './component/tchat/tchat.component';


@NgModule({
    declarations: [
        StreamsDetailsComponent,
        StreamsOfGameComponent,
        ReplaceUrlPipe,
        VideoPlayerComponent,
        StreamViewComponent,
        TchatComponent
    ],
    exports: [
        ReplaceUrlPipe
    ],
  imports: [
    CommonModule,
    StreamsRoutingModule,
    UtilsModule,
    SafePipeModule,
    NgxFlagIconCssModule
  ]
})
export class StreamsModule { }
