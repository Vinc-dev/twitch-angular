export interface User {
  id: number;
  display_name: string;
  description: string;
  profile_image_url: string;
  view_count: number;
  created_at: Date;
}
