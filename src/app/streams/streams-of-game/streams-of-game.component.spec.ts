import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamsOfGameComponent } from './streams-of-game.component';

describe('StreamsOfGameComponent', () => {
  let component: StreamsOfGameComponent;
  let fixture: ComponentFixture<StreamsOfGameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StreamsOfGameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamsOfGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
