import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TwitchService} from '../../api/services/twitch.service';
import {Observable} from 'rxjs';
import {Stream} from '../Stream';
import {Game} from '../../games/Game';

@Component({
  selector: 'twi-streams-of-game',
  templateUrl: './streams-of-game.component.html',
  styleUrls: ['./streams-of-game.component.scss']
})
export class StreamsOfGameComponent implements OnInit {

  streamsForGame$: Observable<Stream[]>;
  game$: Observable<Game>;
  gameTitle: string;
  gameImg: string;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private twitchService: TwitchService
  ) {
  }

  ngOnInit(): void {
    const gameId = this.route.snapshot.paramMap.get('idGame');
    this.game$ = this.twitchService.getOne('/games?id=' + gameId);
    this.game$.subscribe(value => this.gameTitle = value.name);
    this.game$.subscribe(value => this.gameImg = value.box_art_url);
    this.streamsForGame$ = this.twitchService.getAll('/streams?game_id=' + gameId);
  }

}
