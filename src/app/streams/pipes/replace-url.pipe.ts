import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceUrl'
})
export class ReplaceUrlPipe implements PipeTransform {

  transform(imgUrl: string, ...args: string[]): string {
    return imgUrl
      .replace('{width}', args[0])
      .replace('%{width}', args[0])
      .replace('{height}', args[1])
      .replace('%{height}', args[1]);
  }

}
