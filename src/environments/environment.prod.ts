const apiConfig = {
  clientId: 'esa57s92lyftr4lr7wbll04clo6uag',
  authUrl: 'https://id.twitch.tv/oauth2/authorize',
  redirectUrl: 'http://localhost:4200/auth/authorize',
  baseUrl: 'https://api.twitch.tv/helix',
};

const oAuthConfig = {
  authLink: `${apiConfig.authUrl}?client_id=${apiConfig.clientId}&response_type=token&redirect_uri=${apiConfig.redirectUrl}`
};

export const environment = {
  production: true,
  api: {...apiConfig, ...oAuthConfig},
  twitch: {
    videoPlayerUrl: 'https://player.twitch.tv/?channel=[USERNAME]&parent=localhost',
    chatRoomUrl: 'https://www.twitch.tv/embed/[USERNAME]/chat?parent=localhost',
  }
};
